﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Xml.Linq;
using Microsoft.Win32;
using Infragistics.Documents.Excel;
using BobbyTables;
using System.Threading.Tasks;
using PowerKids.Accounting;
using System.Diagnostics;

namespace IGExcel.Samples.Data
{
    /// <summary>
    /// Interaction logic for ExportToExcel.xaml
    /// </summary>
    public partial class ExportToExcel : Window
    {

        const string ACCESS_TOKEN = "aRdmf7-_P6kAAAAAAAAZaZhdk5j7dF6aK8b8w2kzfM289X2Mlv3p6vwKIzehp5Kx";

        public ExportToExcel()
        {
            InitializeComponent();
            this.Loaded += (s,e) => Page_Loaded();
            //PowerKids.Accounting.AsyncHelpers.RunSync(() => PrepareDbx());
        }

        private async Task<IEnumerable<Registration>> PrepareDbx()
        {
            var manager = new DatastoreManager(ACCESS_TOKEN);

            // Getting or creating a datastore
            var datastore = await manager.GetOrCreateAsync("default");

            await datastore.PullAsync();

            IEnumerable<Registration> regList = datastore.GetTable<Registration>("registration");

            return regList;

        }


        private void Page_Loaded()
        {
            CultureInfo cultureInfo = Thread.CurrentThread.CurrentCulture;

            List<string> excelFormats = new List<string>
            {
                "Excel XLS file format",
                "Excel 2007 XLSX file format"
            };

            this.ComboBox_ExcelFormat.ItemsSource = excelFormats;
            this.ComboBox_ExcelFormat.SelectedIndex = 0;
        }

        private void ExportExcel_Click(object sender, RoutedEventArgs e)
        {
            Workbook dataWorkbook = new Workbook();
            Worksheet sheetOne = dataWorkbook.Worksheets.Add("Data Sheet");

            // Export to Excel2007 format 
            if (ComboBox_ExcelFormat.SelectedIndex == 0)
            {
                dataWorkbook.SetCurrentFormat(WorkbookFormat.Excel2007);
            }
            else
            {
                dataWorkbook.SetCurrentFormat(WorkbookFormat.Excel97To2003);
            }

            // Print column header text 
            int currentColumn = 0;
            foreach (DataGridColumn column in this.dataGrid.Columns)
            {
                this.SetCellValue(sheetOne.Rows[0].Cells[currentColumn], column.Header);
                currentColumn++;
            }

            // Export Data From Grid
            IEnumerable gridData = dataGrid.ItemsSource;

            int currentRow = 1;
            WorksheetRow worksheetRow;

            foreach (Registration registration in gridData)
            {
                int currentCell = 0;
                worksheetRow = sheetOne.Rows[currentRow];

                this.SetCellValue(worksheetRow.Cells[currentCell], registration.regid);
                this.SetCellValue(worksheetRow.Cells[++currentCell], registration.name);
                this.SetCellValue(worksheetRow.Cells[++currentCell], registration.parent);
                this.SetCellValue(worksheetRow.Cells[++currentCell], registration.dob);
                this.SetCellValue(worksheetRow.Cells[++currentCell], registration.phone);
                this.SetCellValue(worksheetRow.Cells[++currentCell], registration.start);
                this.SetCellValue(worksheetRow.Cells[++currentCell], registration.expire);
                this.SetCellValue(worksheetRow.Cells[++currentCell], registration.campusid);
                this.SetCellValue(worksheetRow.Cells[++currentCell], registration.campusname);


                currentRow++;
            }

            this.setColumnFormatting(sheetOne);
            this.SaveExport(dataWorkbook);
        }

        private void SetCellValue(WorksheetCell cell, object value)
        {
            cell.Value = value;
            cell.CellFormat.ShrinkToFit = ExcelDefaultableBoolean.True;
            cell.CellFormat.VerticalAlignment = VerticalCellAlignment.Center;
            cell.CellFormat.Alignment = HorizontalCellAlignment.Center;
        }

        private void SaveExport(Workbook dataWorkbook)
        {
            SaveFileDialog dialog;
            Stream exportStream;

            // Export to xlsx excel file format
            if (ComboBox_ExcelFormat.SelectedIndex == 0)
            {
                dialog = new SaveFileDialog { Filter = "Excel files|*.xlsx", DefaultExt = "xlsx" };
            }
            else
            {
                dialog = new SaveFileDialog { Filter = "Excel files|*.xls", DefaultExt = "xls" };
            }

            if (dialog.ShowDialog() == true)
            {
                try
                {
                    exportStream = dialog.OpenFile();
                    dataWorkbook.Save(exportStream);
                    exportStream.Close();
                }
                catch (Exception ex)
                {
                    System.Windows.MessageBox.Show(ex.Message);
                }
            }
        }

        private void setColumnFormatting(Worksheet worksheet)
        {
            // Freeze header row
            worksheet.DisplayOptions.PanesAreFrozen = true;
            worksheet.DisplayOptions.FrozenPaneSettings.FrozenRows = 1;

            // Build Column List
            worksheet.DefaultColumnWidth = 5000;
            worksheet.Columns[1].Width = 8500;
            worksheet.Columns[3].Width = 10000;
        }

        private async void PullData_Click(object sender, RoutedEventArgs e)
        {
            IEnumerable<Registration> regList = await PrepareDbx();
            this.dataGrid.ItemsSource = regList;

        }
    }
}
