﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PowerKids.Accounting
{
    public class Registration
    {
        //public static final String TABLE_NAME = "registration";
        //public static final String COLUMN_NAME_STUDENT_ID = "regid";
        //public static final String COLUMN_NAME_DATE = "date";
        //public static final String COLUMN_NAME_NAME = "name";
        //public static final String COLUMN_NAME_DOB = "dob";
        //public static final String COLUMN_NAME_PARENT = "parent";
        //public static final String COLUMN_NAME_PHONE = "phone";
        //public static final String COLUMN_NAME_CAMPUS_ID = "campusid";
        //public static final String COLUMN_NAME_START = "start";
        //public static final String COLUMN_NAME_EXPIRE = "expire";
        //public static final String COLUMN_NAME_HEIGHT = "height";
        //public static final String COLUMN_NAME_WEIGHT = "weight";
        //public static final String COLUMN_NAME_PULSE = "pulse";
        //public static final String COLUMN_NAME_BLOODPLOW = "bloodplow";
        //public static final String COLUMN_NAME_BLOODPHIGH = "bloodphigh";
        //public static final String COLUMN_NAME_CAMPUS_NAME = "campusname" ;
        //fields.set(RegContract.RegEntry.COLUMN_NAME_STUDENT_ID, RegContract.RegEntry.getId());
        //fields.set(RegContract.RegEntry.COLUMN_NAME_DATE, Calendar.getInstance().getTime());
        //fields.set(RegContract.RegEntry.COLUMN_NAME_NAME, getEmptyText(R.id.text_name));
        //fields.set(RegContract.RegEntry.COLUMN_NAME_DOB, getDateFromPicker(R.id.dob_date));
        //fields.set(RegContract.RegEntry.COLUMN_NAME_PARENT, getEmptyText(R.id.text_parent));
        //fields.set(RegContract.RegEntry.COLUMN_NAME_PHONE, getEmptyText(R.id.text_phone));
        //fields.set(RegContract.RegEntry.COLUMN_NAME_CAMPUS_ID, SettingsHelper.getCampusId(StudentRegistration.this));
        //fields.set(RegContract.RegEntry.COLUMN_NAME_CAMPUS_NAME, SettingsHelper.getCampusName(StudentRegistration.this));
        //fields.set(RegContract.RegEntry.COLUMN_NAME_START, getDateFromPicker(R.id.reg_date));
        //fields.set(RegContract.RegEntry.COLUMN_NAME_EXPIRE, getDateFromPicker(R.id.exp_date));
        //fields.set(RegContract.RegEntry.COLUMN_NAME_HEIGHT, getNumberPickerInt(R.id.height_picker));
        //fields.set(RegContract.RegEntry.COLUMN_NAME_WEIGHT, getNumberPickerInt(R.id.weight_picker));
        //fields.set(RegContract.RegEntry.COLUMN_NAME_PULSE, getNumberPickerInt(R.id.pulse_picker));
        //fields.set(RegContract.RegEntry.COLUMN_NAME_BLOODPLOW, getNumberPickerInt(R.id.blood_low_picker));
        //fields.set(RegContract.RegEntry.COLUMN_NAME_BLOODPHIGH, getNumberPickerInt(R.id.blood_high_picker));

        public string Id;
        public string regid{get;set;}
        public DateTime date { get; set; }
        public string name{get;set;}
        public DateTime dob{get;set;}
        public string parent{get;set;}
        public string phone{get;set;}
        public int campusid{get;set;}
        public string campusname{get;set;}
        public DateTime start{get;set;}
        public DateTime expire{get;set;}
        public int height{get;set;}
        public int weight{get;set;}
        public int pulse{get;set;}
        public int bloodplow{get;set;}
        public int bloodphigh{get;set;}

        public Registration() { }

        public override string ToString()
        {
            return string.Format("{0}-{1}", regid, name);
        }
           
    }
}
